import Component from '@ember/component';
import { computed } from '@ember/object';
//import { htmlSafe } from '@ember/template';

export default Component.extend({
	init: function() {
		this._super(...arguments);
		//this.set('collapsed', true);
	},
	/*
	depthStyle : computed('model.path', function() {
		let path = this.get('model.path')
	
		let count = 0;
		for (var i=0; i < path.length; i++) {
			if (path[i] === '/') {count++}
		}
		
		return htmlSafe("margin-left: " + count + "em");
	}),
	*/
	/*
	icon : computed('collapsed', function() {
		let collapsed = this.get('collapsed')
		return collapsed ? "yellow folder icon" : "yellow folder open icon"
	}),
	*/
	/*
	chevronIcon : computed('collapsed','subKeys', function() {
		let collapsed = this.get('collapsed')
		let children = this.get('subKeys')
		
		return (children && children.length == 0) ? "icon" : (collapsed ? "grey chevron right icon" : "black chevron down icon")
	}),
	*/
	
	values : computed('model.children', function() {
		let children = this.get('model.children')
		let getKey = this.get('getKey') || function() { }
		
		let result = []
		
		if (children) {
			children.forEach(function(item) {
				let k = getKey(item)
				
				if (k.type === "value") {
					result.push(k)
				}
			})
		}
		
		return result
	}),
	
	actions : {
		
	/*
		lookup(path) {
			let getKey = this.get('getKey') || function() { }
			return getKey(path)
		},
		*/
	}
	
	
	
	//let model = this.get('model')
	// getKey
});