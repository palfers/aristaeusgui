import { helper } from '@ember/component/helper';

export function truncateString([p, m, ...rest]/*, hash*/) {
	return (p.length > m) ? p.slice(0,m-3) + "..." : p
}

export default helper(truncateString);