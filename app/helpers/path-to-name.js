import { helper } from '@ember/component/helper';

export function pathToName([p, ...rest]/*, hash*/) {
	let bits = p.split("/")
	return bits[bits.length-1]
}

export default helper(pathToName);