import { helper } from '@ember/component/helper';

export function convertRegistryType([p, ...rest]/*, hash*/) {
	
	let Types = [
		"REG_NONE",
		"REG_SZ",
		"REG_EXPAND_SZ",
		"REG_BINARY",
		"REG_DWORD",
		"REG_DWORD_BIG_ENDIAN",
		"REG_LINK",
		"REG_MULTI_SZ",
		"REG_RESOURCE_LIST",
		"REG_FULL_RESOURCE_DESCRIPTOR",
		"REG_RESOURCE_REQUIREMENTS_LIST",
		"REG_QWORD"
	]
	
	/*
	"REG_BINARY"
	"REG_DWORD"
	"REG_QWORD"
	"REG_DWORD_LITTLE_ENDIAN"
	"REG_QWORD_LITTLE_ENDIAN"
	"REG_DWORD_BIG_ENDIAN"
	"REG_EXPAND_SZ"
	"REG_LINK"
	"REG_MULTI_SZ"
	"REG_NONE"
	"REG_RESOURCE_LIST"
	"REG_SZ"
	*/
	return (p < Types.length) ? Types[p] : "UNKNOWN"
}

export default helper(convertRegistryType);