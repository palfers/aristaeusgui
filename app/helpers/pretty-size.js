import { helper } from '@ember/component/helper';

export function prettySize([p, ...rest]/*, hash*/) {
	let KB = 1024
	let MB = KB*1024

	if (p < KB) {
		return p + "B"
	}
	
	if (p < MB) {
		return p/KB + "KB" 
	}
	
	return p/MB + "MB"
}

export default helper(prettySize);