import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
	showList : computed('model', function() {
		let model = this.get('model')
		console.log(model.content)
		return Array.isArray(model)
		//return model.length > 1
	}),

	actions : {
		lookup(path) {
			let pathMap = this.get('model.map')
			return pathMap[path]
		},
		setSelected(path) {
			let pathMap = this.get('model.map')
			this.set('selected', pathMap[path])
		},
	}
});
