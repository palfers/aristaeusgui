import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
	actions : {
		lookup(path) {
			let pathMap = this.get('model.map')
			return pathMap[path]
		},
		setSelected(path) {
			let pathMap = this.get('model.map')
			this.set('selected', pathMap[path])
		},
		//isSelected(path) {
		//	let p = this.get('selected')
		//	return p === path
		//},
	}
});
