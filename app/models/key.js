import DS from 'ember-data';

export default DS.Model.extend({ 
	path:DS.attr(),
	classname:DS.attr(),
	hash:DS.attr(),
	//	children:[],
	timestamp: DS.attr(),
	nodetype: DS.attr()
});
