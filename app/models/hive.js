import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({ 
	map:DS.attr(),
	label:DS.attr(),
	description:DS.attr(),
	//hash:DS.attr(),
	//	children:[],
	//timestamp: DS.attr(),
	//nodetype: DS.attr()
		
	rootNodes : computed('map', function() {
		
		let pathMap = this.get('map')
		var isRootNode = function(path) {
			for (var i=1; i < path.length; i++) {
				if (path[i] === '/') {
					return false
				}
			}
			return true
		}

		var filtered = Object.keys(pathMap).reduce(function(filtered, key) {
			if (isRootNode(key)) {
				filtered.push(pathMap[key])
			}
			return filtered
		}, [])
		return filtered
	})
});
