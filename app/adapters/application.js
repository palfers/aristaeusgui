import { DocumentAdapter } from 'ember-couch';
 
export default DocumentAdapter.extend({
   host: 'http://localhost:5984',
   db: 'aristaeus',
   designDoc: 'keys',
   typeViewName: 'root',
   
   findRecord: function (store, type, id) {
       var normalizeResponse;
       if (this._checkForRevision(id)) {
           return this.findWithRev(store, type, id);
       } else {
           normalizeResponse = function (data) {
               var _modelJson = {id:data._id};
               this._normalizeRevision(data);
               _modelJson[type.modelName] = data;
               //console.log(_modelJson)
               //return _modelJson;
               data.id = data._id
               return data
           };
           return this.ajax(id, "GET", normalizeResponse);
       }
   },
   
   query: function (store, type, query) {
       var designDoc, normalizeResponse, method;
       designDoc = query.designDoc || this.get("designDoc");
       method = query.method || "GET";

       if (!query.options) {
           query.options = {};
       }
       if (query.ids) {
           return this.findMany(store, type, query.ids, query.options);
       }
       query.options.include_docs = false;
       normalizeResponse = function (data) {
           var json,
               self = this;
           json = {};
           //json[designDoc] = data.rows.getEach("doc").map(function (doc) {
           //json[designDoc] = data.rows.getEach("value").map(function (doc) {
           json.data = data.rows.getEach("value").map(function (doc) {
        	   
               return self._normalizeRevision(doc);
           });
           json.data.forEach(function(value) {
        	  value.type = type 
           });
           json.total_rows = data.total_rows;
           if (query.options.limit) {
               json.total_pages = Math.ceil(data.total_rows / query.options.limit);
           }

           //return json;
           
           return json.data
       };
       return this.ajax("_design/" + designDoc + "/_view/" + query.viewName, method, normalizeResponse, {
           context: this,
           data: query.options
       });
   },
   
});
