import Route from '@ember/routing/route';

export default Route.extend({
	
	model(params) {
		var id = params.hive_id;
		if (id === undefined) {
			return this.get('store').query('hive', {viewName: "labels", designDoc:"hive"})	
		} else {
			return this.get('store').findRecord('hive', id)
		}
		
		//return this.get('store').findAll('SOFTWARE')
		//return $.ajax("http://127.0.0.1:5984/aristaeus/_design/keys/_view/path").dataFilter(function(data,type) {
		//	return 
		//})
	}
});
